package org.example;


import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Task2 {

    static class Employee {
        String title;
        String name;
        String mobile;

        public Employee(String title, String name, String mobile) {
            this.title = title;
            this.name = name;
            this.mobile = mobile;
        }

        public String getTitle() {
            return title;
        }

        public String toString() {
            return this.name + ", " + this.mobile + ".";
        }
    }

    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
                new Employee("ASE", "Ahmed Omran", "01121508662"),
                new Employee("SE", "Yomna Yousry", "01122222222"),
                new Employee("ASE", "Abdelrahman Eltohamy", "01125478999"),
                new Employee("SE", "Hani Zaatar", "01133333333"),
                new Employee("ASE", "Moataz Elsakkar", "01155555555"),
                new Employee("SE", "Hussein Yehya", "01122222233"),
                new Employee("SSE", "Someone", "01188888888")
        );


        employees.stream()
                .collect(Collectors.groupingBy(Employee::getTitle))
                .forEach((String title, List<Employee> employeeGroup) -> {
                    if (employeeGroup.size() < 2) {
                        throw new UncheckedIOException(new IOException("Found Count < 2"));
                    } else {
                        System.out.println("Title " + title + " Count " + employeeGroup.size());
                        employeeGroup.forEach(System.out::println);
                    }
                });

    }
}